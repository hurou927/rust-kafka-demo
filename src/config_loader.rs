use serde::Deserialize;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;

static CONF_FILE_PATH: &str = "./kafka_info.tsv";

#[derive(Debug, Clone)]
pub struct KafkaConf {
    pub brokers: Vec<String>,
    pub topic: String,
}

impl KafkaConf {
    fn new(rows: Vec<Row>) -> Self {
        let brokers: Vec<String> = rows
            .iter()
            .map(|row| format!("{}:{}", row.ip, row.port))
            .collect();

        KafkaConf {
            brokers,
            topic: "foo".to_owned(),
        }
    }
}

#[derive(Debug, Deserialize)]
struct Row {
    name: String,
    ip: String,
    port: String,
}

pub fn load_conf() -> Result<KafkaConf, Box<dyn Error>> {
    let f = File::open(CONF_FILE_PATH).map_err(|err| {
        println!(
            "{} does not exist. Run `make -C kafka info`",
            CONF_FILE_PATH
        );
        err
    })?;
    let reader = BufReader::new(f);
    let mut rdr = csv::ReaderBuilder::new()
        .delimiter(b'\t')
        .from_reader(reader);

    let rows: Vec<Row> = rdr
        .deserialize::<Row>()
        .collect::<Result<Vec<Row>, csv::Error>>()?;

    let kafka_conf = KafkaConf::new(rows);
    println!("load conf: {:?}", kafka_conf);
    Ok(kafka_conf)
}
