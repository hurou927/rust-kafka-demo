mod config_loader;

use std::error::Error;
use std::time::Duration;

use kafka::{
    client::RequiredAcks,
    consumer::Consumer,
    producer::{Producer, Record},
};

use crate::config_loader::{load_conf, KafkaConf};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let kafka_conf = load_conf()?;
    let _result = tokio::join!(
        tokio::spawn(produce(kafka_conf.clone())),
        tokio::spawn(consume(kafka_conf.clone(), 0, 3000)),
        tokio::spawn(consume(kafka_conf.clone(), 1, 5000)),
    );

    Ok(())
}

async fn consume(kafka_conf: KafkaConf, id: i32, sleep_ms: u64) {
    let mut con = Consumer::from_hosts(kafka_conf.brokers)
        .with_topic(kafka_conf.topic)
        .create()
        .unwrap();
    println!("start consume");
    let mut counter: i64 = 0;
    loop {
        counter = counter + 1;
        println!("{}-th poll", counter);
        tokio::time::sleep(Duration::from_millis(sleep_ms)).await;
        let mss = con.poll().unwrap();
        if mss.is_empty() {
            println!("No messages available right now.");
            continue;
        }

        for ms in mss.iter() {
            for m in ms.messages() {
                let value = std::str::from_utf8(m.value).unwrap();
                println!("[CONSUMER {}] value:{:?}, offset: {}", id, value, m.offset);
            }
            let _ = con.consume_messageset(ms);
        }
        con.commit_consumed().unwrap();
    }
}

async fn produce(kafka_conf: KafkaConf) {
    let mut producer = Producer::from_hosts(kafka_conf.brokers)
        .with_ack_timeout(Duration::from_secs(1))
        .with_required_acks(RequiredAcks::One)
        .create()
        .unwrap();
    let key = ();
    let mut counter: i64 = 0;
    loop {
        counter = counter + 1;
        let mut word = String::new();
        std::io::stdin().read_line(&mut word).ok();
        let input = word.trim().to_string();
        let data = format!("{}: {}", counter, input);
        println!("[PRODUCER] produce: {}", data);
        producer
            .send(&Record {
                topic: &kafka_conf.topic,
                partition: -1,
                key,
                value: data.as_bytes(),
            })
            .unwrap();
    }
}
