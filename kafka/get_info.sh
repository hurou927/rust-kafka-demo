#/bin/bash

outputpath=../kafka_info.tsv

containers=$(docker-compose ps | grep start-kafka | awk '{print $1}' | tr "\n" ' ')
echo $containers

# Write Header
printf "name\tip\tport\n" > $outputpath
docker inspect ${containers} \
    | jq -rc '.[] as $r | $r.NetworkSettings as $n | [ $r.Name, ($n.Networks | to_entries[0].value.IPAddress), ($n.Ports | to_entries[0].value[0].HostPort)] | @tsv' >> "$outputpath"

echo output: $outputpath
cat $outputpath

# hostInfo=$(\
#     docker inspect ${containers} \
#     | jq -rc '.[] as $r | $r.NetworkSettings as $n | [ $r.Name, ($n.Networks | to_entries[0].value.IPAddress), ($n.Ports | to_entries[0].value[0].HostPort)]' \
#     )

# echo "HOST INFO"
# echo "$hostInfo"

# echo "RUST"
# echo "$hostInfo" | jq -rc '"\"\(.[1]):\(.[2])\".to_owned(), /\(.[0])"'

