# Kafka Demo

Require: `Cargo`, `Docker`, `docker-compose`

```sh
$ make -C ./kafka scale
$ make -C ./kafka info # show ips and ports
$ cargo run
```

